# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

# Uncomment the following line if you don't like systemctl's auto-paging feature:
# export SYSTEMD_PAGER=

# User specific aliases and functions
function check_tor () {
  wget -q -O - https://check.torproject.org/ | grep -Poz 'IP address appears to be.*</st|class="(off|not)">[^><]+<'
}

function todo_tor () {
  . torsocks on
  check_tor
}

decode () {
  echo "$1" | base64 -d ; echo
}
# pdf a5 to a4
#function pdfa5toa4() {pdftk A=$1 B=$2 cat A1east B1east output - | pdf2ps -dLanguageLevel=3 - - | psnup -2 -Pa5 -pa4 | ps2pdf -dCompatibility=1.4 - $3}

export PATH=~/0xacab/sutty/haini.sh:~/.local/bin:~/bin:$PATH

#function wlinks() {
#  wget -nv -O - $@ | /usr/bin/grep -o "\(https\?://\|magnet:\)[^\"']\+" | sed "s/<[^>]\+>//g"}

#function links() { grep -o "https\?://[^\"']\+" }

alias rm='rm -I'
alias syncthing="/home/librenauta/Syncthing/syncthing-linux-amd64-v1.3.0/syncthing &"
alias expose=/home/librenauta/github/Expose/expose.sh
alias log="/usr/local/bin/Log/Log &"
alias ..='cd ..'
alias hdmi="xrandr --output HDMI-1 --auto --right-of eDP-1"
alias push-copiona="rsync -av _site/ root@copiona.com:/srv/copiona.com"
alias cifrado="udisksctl unlock -b /dev/sdb3"
alias home="udisksctl mount -b /dev/mapper/fedora-home"
alias suspend="systemctl suspend"
alias li="sudo"
alias task="go-task"
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion
# Run twolfson/sexy-bash-prompt
. ~/.bash_prompt
export PATH="$HOME/.rbenv/bin:$PATH"
eval "$(rbenv init - )"
